<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {

//    BOOK FAILURE CONTROLLER
    Route::get('/', 'BookFailruleController@index');
    Route::post('/failuresByDate', 'BookFailruleController@getFailuresByDate')->middleware('isDirektorReferent');
    Route::post('/addNewFailure', 'BookFailruleController@store');
    Route::post('/addDetails', 'BookFailruleController@addDetails')->middleware('isReferent');
    Route::get('/moji-zahtevi', 'BookFailruleController@getAllMyFailures')->middleware('isDirektorReferent');
    Route::post('/search-myfailures', 'BookFailruleController@getAllMyFailuresByDate');

//    PURCHASE REQUEST CONTROLLER
    Route::post('/purchase/request', 'PurchaseRequestController@store');
    Route::get('/zahtev-nabavka', 'PurchaseRequestController@index')->middleware('isReferent');
    Route::get('/nov-zahtev', 'PurchaseRequestController@sendRequest')->middleware('isReferent');
    Route::post('/searchSendRequests', 'PurchaseRequestController@searchSendRequests')->middleware('isDirektorReferent');

//    REPORT PROCUREMENT CONTROLLER
    Route::get('/izvestaj-nabavke', 'ReportProcurementController@index')->middleware('isDirektorRacunovodja');
    Route::post('/failuresByDateReport', 'ReportProcurementController@getFailuresByDateReport')->middleware('isDirektorRacunovodja');
    Route::post('/paidoff', 'ReportProcurementController@paidOff')->middleware('isDirektorRacunovodja');

//    RECOVERY PROPOSAL CONTROLLER
    Route::get('/predlog-oporavka', 'RecoveryProposalController@index')->middleware('isDirektor');
    Route::post('/failuresByDateRecovery', 'RecoveryProposalController@getFailuresByDateRecovery')->middleware('isDirektor');
    Route::post('/statusAnswer', 'RecoveryProposalController@statusAnswer')->middleware('isDirektor');

//    USER CONTROLLER
    Route::get('/korisnici', 'UserController@index')->middleware('isDirektor');
    Route::post('/search-users', 'UserController@searchUsers')->middleware('isDirektor');
    Route::post('/addNewUser', 'UserController@store')->middleware('isDirektor');
    Route::post('/deleteUser/{id}', 'UserController@destroy')->middleware('isDirektor');
    Route::post('/editUser/{id}', 'UserController@update')->middleware('isDirektor');
    Route::get('/settings', 'UserController@settingsIndex')->middleware('isDirektor');
    Route::post('edit/settings', 'UserController@editSettings')->middleware('isDirektor');

//    LIST OF REQUEST CONTROLLER
    Route::get('/narucen-materijal', 'ListOfRequestController@index')->middleware('isDirektorRacunovodja');
    Route::post('/searchOrderedMaterial', 'ListOfRequestController@searchOrderedMaterial')->middleware('isDirektorRacunovodja');
});

Auth::routes();

Route::get('logout','Auth\LoginController@logout');

/**
 * Izvestaj o nabavkama DIREKTOR I RACUNOVODJA
 * Zahtev za nabaku RACUNOVODJA i REFERENT
 * Mesecni pregled DIREKTOR
 * DODAVANJE KORISNIKA DIREKTOR
 */
