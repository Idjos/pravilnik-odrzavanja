<!-- Podesavanja -->
@extends('welcome')

@section('content')
    <div class="col-md-12 title-property-2">
        <h4 class="title-page">Generalna podešavanja sistema</h4>
    </div>
    <div class="col-md-12">
        <form action="edit/settings" method="post" name="">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            @foreach($settings as $setting)
            <input type="hidden" name="id" value="{{$setting->id}}">
            <p>Cena ispod čije vrednosti će svi zahtevi biti prihvaćeni: <input value="{{$setting->default_price}}" name="default_price" class="form-control settings-input input-table-zahtev" type="number" required></p>
            @endforeach
            <button class="btn btn-property-1" type="submit">Sačuvaj</button>
        </form>
    </div>
@endsection
