@extends('welcome')

@section('content')
<div class="container container-log">
    <center>
        <div class="logo-property">
            <img src="img/vtslogo.png">
        </div>
    </center>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="log-form-property panel panel-default">
                <div class="panel-body">
                    <div class="col-md-offset-2 col-md-8 title-property">
                        <h3>VTŠ Održavanje</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-offset-1 col-md-10">
                                <input id="email" type="email" class="form-control input-property-1" name="email" placeholder="E-mail" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-offset-1 col-md-10">
                                <input id="password" type="password" placeholder="Lozinka" class="form-control input-property-1" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="forgot-password col-md-offset-1 col-md-10">
                            <p><a href="/password/reset">Zaboravili ste lozinku?</a></p>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-10 down-margin">
                                <button type="submit" class="btn btn-block btn-property-1">
                                    Prijavi se
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
