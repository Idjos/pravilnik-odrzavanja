<!-- Zahtevi za nabavku -->
@extends('welcome')
@section('content')
    <div class="knjiga-kvarova-content">
        <div class="row title-page">
            <div class="col-md-10 title-property-2">
                <h4>Lista zahteva za period od {{Carbon\Carbon::parse($start_date)->format('d.m.Y')}} do {{Carbon\Carbon::parse($end_date)->format('d.m.Y')}}</h4>
            </div>
            <div class="col-md-1 btn-print-position">
                <button type="button" class="btn hidden-print print-btn" onclick="window.print();"><i class="fa fa-print"></i></button>
            </div>
            <div class="col-md-1 btn-plus hidden-print btn-add-position btn-edit">
                <a class="btn" role="button" href="/nov-zahtev"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="row">
            <form action="/searchSendRequests" method="post" name="failures-by-date-recovery" class="hidden-print">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon input-icon-property-2">Od</span>
                            <input type="date" class="form-control input-property-2" name="start_date" value="{{$start_date}}">
                        </div>
                    </div>
                    <div class="col-md-3 margin-media">
                        <div class="input-group">
                            <span class="input-group-addon input-icon-property-2">Do</span>
                            <input type="date" class="form-control input-property-2" name="end_date" value="{{$end_date}}">
                        </div>
                    </div>
                    <div class="col-md-3 margin-media">
                        <div class="form-group">
                            <input type="text" class="form-control input-search-1" name="search" placeholder="Pretraži" value="{{$search}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-form-property search-property-2">Primeni</button>
                    </div>
            </form>
        </div>
        <table class="table table-bordered" style="margin-top: 20px;" id="example1">
            @if(!$procurement->isEmpty()) {{--IF IS EMPTY--}}
            <thead>
            <tr>
                <th class="cursor-table">Redni broj</th>
                <th class="cursor-table">Datum i vreme</th>
                <th class="cursor-table">Naziv zahteva</th>
                <th class="hidden-print">Odgovor</th>
            </tr>
            </thead>
            <tbody>
            @foreach($procurement as $procurement)
                <tr>
                    <td class="centar-text">{{$procurement->id}}</td>
                    <td>{{$procurement->created_at->format('d.m.Y H:i')}}</td>
                    <td>{{$procurement->name}}</td>
                    <td class="hidden-print">
                        <center>
                            <button type="button" class="btn btn-table-property" style="width: 143px" data-toggle="modal"
                                    data-target="#myModal{{$procurement->id}}">Detalji
                            </button>
                        </center>
                    </td>
                </tr>
                <!-- Modal dodaj novi-->
                <div id="myModal{{$procurement->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Odgovor na zahtev</h4>
                            </div>
                            <div class="modal-body">
                                @foreach($procurement->reportProcurements as $item)
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <div class="row">
                                                        <div style="margin-bottom: 20px;" class="title-collapse col-md-12">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$item->id}}">
                                                                {{str_limit($item->description, $limit = 30, $end = '...')}}
                                                            </a>
                                                        </div>
                                                        <div class="col-md-12 modal-price">
                                                            {{number_format($item->total, 2)}} din
                                                        </div>
                                                    </div>
                                                </h4>
                                            </div>
                                            <div id="collapse{{$item->id}}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <input type="hidden" name="id[]" value="{{$item->id}}">
                                                    <div><b>Opis kvara: </b>{{$item->description}}</div>
                                                    <div><b>Merna jedinica: </b>{{$item->measurement_unit}}</div>
                                                    <div><b>Količina: </b>{{$item->amount}}</div>
                                                    <div><b>Cena po komadu: </b>{{number_format($item->price, 2)}}</div>
                                                    <div><b>Ukupna cena: </b>{{number_format($item->total, 2)}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="modal-footer">
                                    <div class="row">
                                        <button type="button" class="btn modal-button-1" data-dismiss="modal">Zatvori</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>
        @else {{--IF IS EMPTY ELSE--}}
        <center class="hidden-print"><h3 style="padding-top: 20px;">NEMA REZULTATA</h3></center>
        @endif {{--IF IS EMPTY END--}}
    </div>

    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                paging: false,
                filter: false,
                order: [1, 'Datum i vreme'],
                // scrollX: true
            });
        });
    </script>
@endsection
