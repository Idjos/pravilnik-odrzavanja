<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pravilnik održavanja</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        {{--Bootstrap--}}
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

        {{--Font awesome--}}
        <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        {{--CSS--}}
        <link rel="stylesheet" type="text/css" href="bower_components/toastr/toastr.min.css">
        <link href="css/style.css" rel="stylesheet" type="text/css">

        <script src="bower_components/jquery/dist/jquery.js"></script>
    </head>
    <body>
        @if(Auth::check())
        <div class="header hidden-print">
            <nav class="navbar navbar-default nav-bg-property2">
                <div class="container-fluid nav-bg-property">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img class="logo-image" src="img/vtslogo.png"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="{{ Request::is( '/' ) || Request::is( 'failuresByDate' ) ? 'active' : '' }}"><a href="/">Prijavljeni kvarovi</a></li>
                            @if(Auth::user()->role == 'DIREKTOR' || Auth::user()->role == 'REFERENT' || Auth::user()->role == 'IT-SLUŽBA')
                                <li class="{{ Request::is( 'moji-zahtevi' ) || Request::is( 'search-myfailures' )? 'active' : '' }}"><a href="/moji-zahtevi">Moji kvarovi</a></li>
                            @endif
                            @if(Auth::user()->role == 'REFERENT' || Auth::user()->role == 'IT-SLUŽBA')
                                <li class="{{ Request::is( 'zahtev-nabavka' ) ? 'active' : '' || Request::is( 'searchSendRequests' )? 'active' : '' || Request::is( 'nov-zahtev' )? 'active' : ''  }}"><a href="/zahtev-nabavka">Zahtevi za nabavku</a></li>
                            @endif
                            @if(Auth::user()->role == 'DIREKTOR')
                                <li class="{{ Request::is( 'predlog-oporavka' ) || Request::is( 'failuresByDateRecovery' ) ? 'active' : '' }}"><a href="/predlog-oporavka">Lista zahteva</a></li>
                            @endif
                            @if(Auth::user()->role == 'DIREKTOR' || Auth::user()->role == 'RAČUNOVOĐA')
                                <li class="{{ Request::is( 'izvestaj-nabavke' ) || Request::is( 'failuresByDateReport' ) ? 'active' : '' }}"><a href="/izvestaj-nabavke">Zahtevi za isplatu</a></li>
                            @endif
                            @if(Auth::user()->role == 'DIREKTOR' || Auth::user()->role == 'RAČUNOVOĐA')
                                <li class="{{ Request::is( 'narucen-materijal' ) || Request::is('searchOrderedMaterial') ? 'active' : '' }}"><a href="/narucen-materijal">Mesečni izveštaj o nabavkama</a></li>
                            @endif
                            @if(Auth::user()->role == 'DIREKTOR')
                                <li class="{{ Request::is( 'korisnici' ) || Request::is( 'search-users' ) ? 'active' : '' }}"><a href="/korisnici">Korisnici</a></li>
                            @endif
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown dropdown-property-1">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-property-1">
                                    @if(Auth::user()->role == 'DIREKTOR')
                                    <li><a href="/settings">Podešavanja</a></li>
                                    @endif
                                    <li><a href="/logout">Odjavi se</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
        @endif
        <div class="container content-container animation-fade" style="margin-bottom: 40px">
            @yield('content')
        </div>
    </body>

    {{--Bootstrap--}}
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/toastr/toastr.min.js"></script>

    <script>
        toastr.options = {
            "positionClass": "toast-bottom-right"
        }
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
                toastr.success("{{Session::get('message')}}");
            @else
                toastr.error("{{Session::get('message')}}");
            @endif
        @endif
    </script>

    <script>
        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).find('form')[0].reset();
        });
    </script>
</html>
