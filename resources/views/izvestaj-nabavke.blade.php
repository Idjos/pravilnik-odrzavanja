<!-- Izvestaj nabavke -->
@extends('welcome')
@section('content')
    <div class="knjiga-kvarova-content">
        <div class="row title-page">
            <div class="col-md-10 title-property-2">
                <h4>Narudžbine za period od {{Carbon\Carbon::parse($start_date)->format('d.m.Y')}} do {{Carbon\Carbon::parse($end_date)->format('d.m.Y')}}</h4>
            </div>
            <div class="col-md-2 btn-print-position">
                <button type="button" class="btn hidden-print print-btn" onclick="window.print();"><i class="fa fa-print"></i></button>
            </div>
        </div>
        <div class="row hidden-print title-page">
            <form action="/failuresByDateReport" method="post" name="failures-by-date-report">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon input-icon-property-2">Od</span>
                        <input type="date" class="form-control input-property-2" name="start_date" value="{{$start_date}}">
                    </div>
                </div>
                <div class="col-md-3 margin-media">
                    <div class="input-group">
                        <span class="input-group-addon input-icon-property-2">Do</span>
                        <input type="date" class="form-control input-property-2" name="end_date" value="{{$end_date}}">
                    </div>
                </div>
                <div class="col-md-3 margin-media">
                    <div class="form-group">
                        <input type="text" class="form-control input-search-1" name="search" placeholder="Pretraži" value="{{$search}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-form-property search-property-2">Primeni</button>
                </div>
            </form>

        </div>
        {{--IF IS EMPTY--}}
        @if(!$reports->isEmpty())
        @foreach($reports as $report)
        <div class="row">
            <div class="col-md-7">
                <p>Ime zahteva je: {{$report->name}}</p>
            </div>
        </div>
        <form class="form-property" action="/paidoff" method="post" name="failures-by-date-report">
            <table border="1" id="printTable" class="table table-bordered">
                <thead class="head-style">
                <tr>
                    <th class="cursor-table">Ime i prezime podnosioca</th>
                    <th class="cursor-table">Detalji</th>
                    <th class="cursor-table">Količina</th>
                    <th class="cursor-table">Iznos</th>
                    <th class="cursor-table">Ukupan iznos</th>
                    <th class="hidden cursor-table">Total</th>
                    <th class="cursor-table hidden-print">
                        <select class="form-control dropdown-table-zahtev" id="sel{{$report->id}}" onclick="changeValue({{$report->id}})">
                            <option hidden>Isplati sve</option>
                            <option value="ISPLAĆENO">Isplaćeno</option>
                            <option value="ODOBRENO">Nije isplaćeno</option>
                            </optgroup>
                        </select>
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($report->reportProcurements as $item)
                    <input class="hidden" name="id[]" value="{{$item->id}}">
                    <input class="hidden" name="_token" value="{{ csrf_token() }}">
                    <tr>
                        <td width="20%">{{$report->user->name}}</td>
                        <td width="35%">{{$item->description}}</td>
                        <td width="10%" class="amount centar-text">{{$item->amount}}</td>
                        <td width="10%" class="price centar-text">{{number_format($item->price, 2)}}</td>
                        <td width="10%" class="centar-text">{{number_format($item->total, 2)}}</td>
                        <td width="15%" class="hidden-print">
                            <select name="status[]" class="form-control dropdown-table-zahtev value{{$report->id}}" id="">
                                <option @if($item->status == 'ISPLAĆENO') {{'selected'}} @endif value="ISPLAĆENO">Isplaćeno</option>
                                <option @if($item->status != 'ISPLAĆENO') {{'selected'}} @endif value="ODOBRENO">Nije isplaćeno</option>
                            </select>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tr class="hidden-print">
                    <td colspan="4">Ukupna cena</td>
                    <td colspan="1" class="centar-text">{{number_format($report->total, 2)}}</td>
                    <td colspan="1"><center><button class="btn btn-table-property-no-radius" type="submit">Sačuvaj</button></center></td>
                </tr>
            </table>
        </form>
        @endforeach
        @else {{--IF IS EMPTY ELSE--}}
        <center class="hidden-print"><h3>NEMA REZULTATA</h3></center>
        @endif {{--IF IS EMPTY END--}}
    </div>

    <script>
        $(document).ready(function () {
            $('#printTable').DataTable({
                paging: false,
                filter: false,
                order: [1, 'Redni broj'],
                // scrollX: true
            });
        });
    </script>

    <script>
        function changeValue(id) {
            var value = $('#sel'+id).val();
            if (value == 'ODOBRENO') {
                $('.value'+id).val('ODOBRENO');
            }
            else {
                $('.value'+id).val('ISPLAĆENO');
            }
        }
    </script>
@endsection
