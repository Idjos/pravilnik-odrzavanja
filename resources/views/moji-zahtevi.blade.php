<!-- Moji zahtevi -->
@extends('welcome')
@section('content')
    <div class="knjiga-kvarova-content">
        <div class="row title-page">
            <div class="col-md-10 title-property-2">
                <h4>Prijavljeni kvarovi za period od {{Carbon\Carbon::parse($start_date)->format('d.m.Y')}} do {{Carbon\Carbon::parse($end_date)->format('d.m.Y')}}</h4>
            </div>
            @if(Auth::user()->role != 'ZAPOSLENI' && !$book_failures->isEmpty())
            <div class="col-md-1 btn-print-position">
                <button type="button" class="btn hidden-print print-btn" onclick="window.print();"><i class="fa fa-print"></i></button>
            </div>
            <div class="col-md-1 hidden-print btn-add-position">
                <button class="btn btn-plus" type="button" name="button" data-toggle="modal" data-target="#addNew"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
            @else
            <div class="col-md-1 hidden-print btn-add-position">
                <button class="btn btn-plus" type="button" name="button" data-toggle="modal" data-target="#addNew"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
            @endif
        </div>
        <div class="row">
            <form action="/search-myfailures" method="post" name="failures-by-date" class="hidden-print">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon input-icon-property-2">Od</span>
                        <input type="date" class="form-control input-property-2" name="start_date" value="{{$start_date}}">
                    </div>
                </div>
                <div class="col-md-3  margin-media">
                    <div class="input-group">
                        <span class="input-group-addon input-icon-property-2">Do</span>
                        <input type="date" class="form-control input-property-2" name="end_date" value="{{$end_date}}">
                    </div>
                </div>
                <div class="col-md-3  margin-media">
                    <div class="form-group">
                        <input type="text" class="form-control input-search-1" name="search" placeholder="Pretraži" value="{{$search}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-form-property search-property-2">Primeni</button>
                </div>
            </form>
        </div>
        <table class="table table-bordered" id="example">
            @if(!$book_failures->isEmpty())
                <thead>
                <tr>
                    <th class="cursor-table">Redni broj</th>
                    <th class="cursor-table">Datum i vreme</th>
                    <th class="cursor-table">Opis</th>
                    <th class="cursor-table">Lokacija</th>
                    <th class="cursor-table">Ime i prezime podnosioca</th>
                    <th class="cursor-table">Status</th>
                    <th class="hidden-print">Detalji</th>
                </tr>
                </thead>
            @endif
            <tbody>
            @foreach($book_failures as $book_failure)
                <tr>
                    <td class="centar-text">{{$book_failure->id}}</td>
                    <td>{{$book_failure->created_at->format('d.m.Y H:i')}}</td>
                    <td>{{$book_failure->description}}</td>
                    <td>{{$book_failure->location}}</td>
                    <td>{{$book_failure->user['name']}}</td>
                    <td class="status-glitch">
                        <h4>
                            <span class="label label-default"
                            @if($book_failure->status == 'POSLATO') style="background: #004ebe; border-radius:12px; font-size: 10px; font-weight: 500; padding: 5px 12px 5px 12px; letter-spacing: 1px;"
                            @elseif($book_failure->status == 'REŠENO') style="background: #43b78c; border-radius:12px; font-size: 10px; font-weight: 500; padding: 5px 12px 5px 12px; letter-spacing: 1px;"
                            @elseif($book_failure->status == 'NIJE REŠENO') style="background: #e23f56; border-radius:12px; font-size: 10px; font-weight: 500; padding: 5px 12px 5px 12px; letter-spacing: 1px;"
                            @else style="background: #a1a7bd; border-radius:12px; font-size: 10px; font-weight: 500; padding: 5px 12px 5px 12px; letter-spacing: 1px;" @endif>{{$book_failure->status}}
                            </span>
                        </h4>
                    </td>
                    <td class="hidden-print">
                        <center>
                            <button type="button" class="btn btn-table-property" data-toggle="modal" data-target="#myModal{{$book_failure->id}}">
                                Detaljnije
                            </button>
                        </center>
                    </td>
                </tr>
                <!-- Modal Detalji-->
                <div id="myModal{{$book_failure->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Detalji o kvaru</h4>
                            </div>
                                <div class="modal-body">
                                    <div>
                                        <p><b>Ime podnosioca: </b>{{$book_failure->user['name']}}</p>
                                    </div>
                                    <div>
                                        <p><b>Lokacija: </b>{{$book_failure->location}}</p>
                                    </div>
                                    <div>
                                        <p><b>Opis: </b>{{$book_failure->description}}</p>
                                    </div>
                                    <div>
                                        <p><b>Datum: </b>{{$book_failure->created_at->format('d.m.Y H:i')}}</p>
                                    </div>
                                    <hr>
                                    <div>
                                        <label for="comment">Šta je urađeno?</label>
                                        <textarea class="form-control modal-input" rows="5" id="comment" disabled="true" required>{{$book_failure->work_description}}</textarea>
                                    </div>
                                    <div class="comments-what-is-done">
                                        <label for="comment">Šta je ugrađeno?</label>
                                        <textarea class="form-control modal-input" rows="5" id="comment" disabled="true" required>{{$book_failure->work_implemented_parts}}</textarea>
                                    </div>
                                    <div class="comments-what-is-done">
                                        <label for="comment">Primedba</label>
                                        <textarea class="form-control modal-input" rows="5" id="comment" disabled="true" required>{{$book_failure->work_comment}}</textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn modal-button-1" data-dismiss="modal">Zatvori</button>
                                </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>
        @if($book_failures->isEmpty())
            <center class="hidden-print"><h3>NEMA REZULTATA</h3></center>
        @endif
    </div>

    <!-- Modal dodaj novi-->
    <div id="addNew" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Prijavi novi kvar</h4>
                </div>
                <form action="/addNewFailure" method="post" name="add-new-failure">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body">
                        <div>
                            <label for="comment">Lokacija</label>
                            <input type="text" class="form-control modal-input" id="usr" name="location" required>
                        </div>
                        <div class="comments-what-is-done">
                            <label for="comment">Opis</label>
                            <textarea class="form-control modal-input" rows="3" id="comment" name="description" required></textarea>
                        </div>
                        <div class="dropdown comments-what-is-done">
                            <label>Zadužen</label>
                            <select name="experts" class="form-control modal-dropdown" required>
                                <option value="" hidden>Izaberite zaduženo lice</option>
                                <option>IT</option>
                                <option>ODRŽAVANJE</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button class="btn modal-button-2" type="submit">Prijavi</button>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button type="button" class="btn modal-button-1" data-dismiss="modal">Otkaži</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script>
        $( "#start_loading" ).click(function() {
            $('#addNew').modal('toggle');
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                paging: false,
                filter: false,
                order: [1, 'Datum i vreme'],
                // scrollX: true
            });
        } );
    </script>
@endsection
