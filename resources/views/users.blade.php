<!-- Korisnici -->
@extends('welcome')
@section('content')
    <div class="row title-page">
        <div class="col-md-10 title-property-2">
            <h4>Lista svih korisnika</h4>
        </div>
        <div class="col-md-2">
            <div class="hidden-print btn-add-position">
                <button class="btn btn-plus" type="button" name="button" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
            <!-- <div class="btn-plus">
                <i class="fa fa-plus-circle" aria-hidden="true" data-toggle="modal" data-target="#myModal"></i>
            </div> -->
        </div>
    </div>
    <form action="/search-users" method="post" name="search-users-form" class="hidden-print">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row title-page">
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control input-search-1" name="search" placeholder="Pretraži" value="{{$search}}">
                </div>
            </div>
            <div class="col-md-3 margin-media">
                <button type="submit" class="btn btn-form-property search-property-2">Primeni</button>
            </div>
        </div>
    </form>
    <table class="table table-bordered" id="example2">
        <thead>
            <tr>
                <th class="cursor-table">Redni broj</th>
                <th class="cursor-table">Ime i prezime</th>
                <th class="cursor-table">E-mail</th>
                <th class="cursor-table">Nivo pristupa</th>
                <th class="cursor-table">Izmeni informacije</th>
                <th class="cursor-table">Brisanje korisnika</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td class="centar-text">{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role}}</td>
                <td><center><button class="btn btn-table-property btn-property" data-toggle="modal" data-target="#editModal{{$user->id}}">Izmeni</button></center></td>
                <form action="/deleteUser/{{$user->id}}" name="delte-user" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <td><center><button type="submit" class="btn btn-table-property btn-property">Obriši</button></center></td>
                </form>
            </tr>
            <!-- Modal izmeni-->
            <div id="editModal{{$user->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Dodaj novog korisnika</h4>
                        </div>
                        <form action="/editUser/{{$user->id}}" method="post" name="">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="modal-body">
                                <div>
                                    <label for="comment">Ime i prezime</label>
                                    <input type="text" class="form-control modal-input" name="name" value="{{$user->name}}" required>
                                </div>
                                <div class="comments-what-is-done">
                                    <label for="comment">E-mail</label>
                                    <input class="form-control modal-input" type="email" name="email" value="{{$user->email}}" required>
                                </div>
                                <div class="comments-what-is-done">
                                    <label for="comment">Password</label>
                                    <input class="form-control modal-input" type="password" name="password" placeholder="*********">
                                </div>
                                <div class="dropdown comments-what-is-done">
                                    <label>Role</label>
                                    <select name="role" class="form-control modal-dropdown" required>
                                        <option @if($user->role == 'ZAPOSLENI') selected @endif value="ZAPOSLENI">ZAPOSLENI</option>
                                        <option @if($user->role == 'REFERENT') selected @endif value="REFERENT">REFERENT</option>
                                        <option @if($user->role == 'IT-SLUŽBA') selected @endif value="IT-SLUŽBA">IT-SLUŽBA</option>
                                        <option @if($user->role == 'RAČUNOVOĐA') selected @endif value="RAČUNOVOĐA">RAČUNOVOĐA</option>
                                        <option @if($user->role == 'DIREKTOR') selected @endif value="DIREKTOR">DIREKTOR</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button class="btn modal-button-2" type="submit" name="">Sačuvaj</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button type="button" class="btn modal-button-1" data-dismiss="modal">Otkaži</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        @endforeach
        </tbody>
    </table>

    <!-- Modal dodaj novi-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dodaj novog korisnika</h4>
                </div>
                <form action="/addNewUser" method="post" name="add-new-user">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body">
                        <div>
                            <label for="comment">Ime i prezime</label>
                            <input type="text" class="form-control modal-input" name="name" required>
                        </div>
                        <div class="comments-what-is-done">
                            <label for="comment">E-mail</label>
                            <input class="form-control modal-input" type="email" name="email" required>
                        </div>
                        <div class="comments-what-is-done">
                            <label for="comment">Password</label>
                            <input class="form-control modal-input" type="password" name="password" required>
                        </div>
                        <div class="dropdown comments-what-is-done">
                            <label>Role</label>
                            <select name="role" class="form-control modal-dropdown" required>
                                <option value="ZAPOSLENI">ZAPOSLENI</option>
                                <option value="REFERENT">REFERENT</option>
                                <option value="IT-SLUŽBA">IT-SLUŽBA</option>
                                <option value="RAČUNOVOĐA">RAČUNOVOĐA</option>
                                <option value="DIREKTOR">DIREKTOR</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button class="btn modal-button-2" type="submit" name="">Sačuvaj</button>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button type="button" class="btn modal-button-1" data-dismiss="modal">Otkaži</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#example2').DataTable( {
                paging: false,
                filter: false,
                order: [0, 'Redni broj'],
                // scrollX: true
            });
        } );
    </script>
@endsection
