<!-- Nov zahtev -->
@extends('welcome')
@section('content')
    <div class="back-property">
        <a class="" href="/zahtev-nabavka"><i class="fa fa-angle-left"></i> Vratite se nazad</a>
    </div>
    <form action="/purchase/request" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <table class="table table-bordered">
            <tbody>
            <tr>
                @if($list_of_request == null)
                    <td colspan="2">
                        <div class="input-group" style="width: 100%">
                            <span class="input-group-addon input-icon-property-2" id="basic-addon1">Zahtev broj 1</span>
                            <input name="name" aria-describedby="basic-addon1" type="text" class="form-control input-property-2" value="" placeholder="Ime zahteva" required>
                        </div>
                    </td>
                @else
                    <td colspan="2">
                        <div class="input-group" style="width: 100%">
                            <span class="input-group-addon input-icon-property-2" id="basic-addon1">Zahtev broj {{(int)$list_of_request->id+1}}</span>
                            <input name="name" aria-describedby="basic-addon1" type="text" class="form-control input-property-2" value="" placeholder="Ime zahteva" required>
                        </div>
                    </td>
                @endif
                    <td colspan="2" class="colspan-lines-r">
                        <span>Datum: {{\Carbon\Carbon::now()->format('d.m.Y')}}</span>
                    </td>
                    <td colspan="2" class="colspan-lines-l">
                        <button type="button" class="btn btn-plus-2" id="add"><i class="fa fa-plus"></i></button>
                    </td>
            </tr>
            <tr>
                <td class="centar-table">Opis (naziv i/ili oznaka proizvoda/usluge)</td>
                <td class="centar-table">Odabrati prijavu za koju je vezana nabavka</td>
                <td class="centar-table">Jed. mere</td>
                <td class="centar-table">Količina</td>
                <td class="centar-table">Cena</td>
                <td class="centar-table">Ukupna cena</td>
            </tr>
            <tr id="zahtev-table1">
                <td><input class="input-z form-control input-table-zahtev" type="text" name="description[]" required></td>
                <td>
                    <select name="book_failure_id[]" class="form-control dropdown-table-zahtev">
                        @foreach($book_failures as $book_failure)
                            <option value="{{$book_failure->id}}">{{ str_limit($book_failure->description, $limit = 100, $end = '...') }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input class="input-z form-control input-table-zahtev" type="text" name="measurement_unit[]" required></td>
                <td><input class="input-z form-control input-table-zahtev" type="number" id="amount" name="amount[]" required></td>
                <td><input class="input-z form-control input-table-zahtev" type="number" id="price" name="price[]" required></td>
                <td><input class="total input-z form-control input-table-zahtev" disabled id="total" type="number" name="total[]"></td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td colspan="1" class="total-price" id="all_total">0</td>
            </tr>
            <tr>
                <td colspan="6"><button onclick="deleteAll()" type="submit" class="btn btn-table-3">Pošalji</button></td>
            </tr>
            </tbody>
        </table>
    </form>
    <script>
        var i = 2;
        var k = 1;
        var niz=[];
        var n = 0;

        $('form').bind("keypress", function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });

        $(document).ready(function () {

            $('#add').click(function () {
                $('#zahtev-table' + k).after('<tr id="zahtev-table'+ i +'"><td><input class="input-z form-control input-table-zahtev" type="text" name="description[]" required></td><td><select name="book_failure_id[]" class="form-control dropdown-table-zahtev">@foreach($book_failures as $book_failure)<option value="{{$book_failure->id}}">{{ str_limit($book_failure->description, $limit = 100, $end = '...') }}</option>@endforeach</select></td><td><input type="text" class="input-z form-control input-table-zahtev" name="measurement_unit[]" required></td><td><input  class="input-z form-control input-table-zahtev" type="number" name="amount[]" id="amount" required></td><td><input  class="input-z form-control input-table-zahtev" type="number" name="price[]" id="price" required></td><td><input class="total'+i+' input-z form-control input-table-zahtev" disabled id="total" type="number"></td><td><button type="button" class="btn" onclick="deleteItem('+k+')" id="delete"><i class="fa fa-minus"></i></button></td></tr>');
                i++;
                k++;
            });

            $("body").on('keyup', '.input-z', function(){
                var total = [];
                $('input[name^="amount"]').each(function (i) {
                    total.push($('input[name^="amount"]').eq(i).val() * $('input[name^="price"]').eq(i).val())
                });
                $('input[name^="amount"]').each(function (i) {
                    if (i==0) {
                        $('.total').val(total[0])
                    }
                    else {
                        $('.total'+(i+1)).val(total[i])
                    }
                });
            });

            $("body").on('keyup', '.input-z', function(){
                var total = 0;
                $('input[name^="amount"]').each(function (i) {
                    total += $('input[name^="amount"]').eq(i).val() * $('input[name^="price"]').eq(i).val()
                });
                document.getElementById('all_total').innerHTML = total;
            });
        });

        function deleteItem(k) {
            var newK = k+1;
            $('#zahtev-table' + newK).hide();
            niz.push(newK);
            n++;
            // this.k--;
            // this.i--;
        }

        function deleteAll() {
            for (var i=0;i<=n;i++) {
                $('#zahtev-table' + niz[i]).remove();
            }
        }

    </script>
@endsection
