<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(BookFailuresTableSeeder::class);
        $this->call(ListOfRequestsTableSeeder::class);
        $this->call(ReportProcurementsTableSeeder::class);
        $this->call(DefaultAndGeneralsTableSeeder::class);
    }
}
