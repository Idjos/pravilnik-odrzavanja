<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookFailuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['POSLATO', 'REŠENO', 'NIJE REŠENO', 'NEOSNOVAN'];
        $dummy_status2 = ['IT', 'ODRŽAVANJE'];
        for($i = 0; $i <= 50; $i++) {
            DB::table( 'book_failures' )->insert( [
                'description' => $faker->unique()->text( $maxNbChars = 40 ),
                'location' => $faker->unique()->text( $maxNbChars = 40 ),
                'user_id' => rand( 1, 20 ),
                'status' => $dummy_status[rand( 0, 3 )],
                'experts' => $dummy_status2[rand( 0, 1 )],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ] );
        }
    }
}
