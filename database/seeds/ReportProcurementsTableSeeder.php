<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReportProcurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['ČEKA SE ODGOVOR', 'ODOBRENO', 'NIJE ODOBRENO', 'ISPLAĆENO'];
        for($i = 0; $i <= 50; $i++) {
            DB::table( 'report_procurements' )->insert( [
                'description' => $faker->unique()->text( $maxNbChars = 40 ),
                'measurement_unit' => $faker->unique()->text( $maxNbChars = 10 ),
                'book_failure_id' => rand( 1, 10 ),
                'user_id' => rand( 1, 10 ),
                'status' => $dummy_status[rand( 0, 3 )],
                'amount' => rand( 0, 4 ),
                'list_of_request_id' => rand( 1, 10 ),
                'price' => rand(100, 2000),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ] );
        }
    }
}
