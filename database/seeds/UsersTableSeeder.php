<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['DIREKTOR', 'REFERENT', 'ZAPOSLENI', 'RAČUNOVOĐA', 'IT-SLUŽBA'];
        for($i = 0; $i <= 50; $i++) {
            DB::table( 'users' )->insert( [
                'name' => $faker->unique()->text( $maxNbChars = 10 ),
                'role' => $dummy_status[rand( 0, 3 )],
                'email' => $faker->unique()->email(),
                'password' => bcrypt('123'),
            ] );
        }
        DB::table( 'users' )->insert( [
            'name' => 'direktor',
            'role' => 'DIREKTOR',
            'email' => 'direktor@demo.com',
            'password' => bcrypt('123'),
        ] );
        DB::table( 'users' )->insert( [
            'name' => 'referent',
            'role' => 'REFERENT',
            'email' => 'referent@demo.com',
            'password' => bcrypt('123'),
        ] );
        DB::table( 'users' )->insert( [
            'name' => 'zaposleni',
            'role' => 'ZAPOSLENI',
            'email' => 'zaposleni@demo.com',
            'password' => bcrypt('123'),
        ] );
        DB::table( 'users' )->insert( [
            'name' => 'racunovodja',
            'role' => 'RAČUNOVOĐA',
            'email' => 'racunovodja@demo.com',
            'password' => bcrypt('123'),
        ] );
        DB::table( 'users' )->insert( [
            'name' => 'itsluzba',
            'role' => 'IT-SLUŽBA',
            'email' => 'it@demo.com',
            'password' => bcrypt('123'),
        ] );

        DB::table( 'users' )->insert( [
            'name' => 'Slavimir Stošović',
            'role' => 'DIREKTOR',
            'email' => 'slavimir.stosovic@vtsnis.edu.rs',
            'password' => bcrypt('123'),
        ] );
    }
}
