<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListOfRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status2 = ['IT', 'ODRŽAVANJE'];
        for($i = 0; $i <= 50; $i++) {
            DB::table( 'list_of_requests' )->insert( [
                'name' => $faker->name(),
                'user_id' => rand( 1, 10 ),
                'experts' => $dummy_status2[rand( 0, 1 )],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ] );
        }
    }
}
