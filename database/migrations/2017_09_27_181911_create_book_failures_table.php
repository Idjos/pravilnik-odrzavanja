<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookFailuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_failures', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->string('location');
            $table->integer('user_id')->unsigned();
            $table->enum('status', ['POSLATO', 'REŠENO', 'NIJE REŠENO', 'NEOSNOVAN']);
            $table->enum('experts', ['IT', 'ODRŽAVANJE']);

            $table->text('work_description')->nullable();
            $table->text('work_implemented_parts')->nullable();
            $table->text('work_comment')->nullable();
            $table->timestamps();
        });

        Schema::table('book_failures', function (Blueprint $table) {
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_failures');
    }
}
