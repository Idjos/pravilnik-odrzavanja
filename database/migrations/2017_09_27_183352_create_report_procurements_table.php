<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportProcurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_procurements', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->string('measurement_unit');
            $table->integer('list_of_request_id')->unsigned();
            $table->integer('book_failure_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->enum('status', ['ČEKA SE ODGOVOR', 'ODOBRENO', 'NIJE ODOBRENO', 'ISPLAĆENO']);
            $table->integer('amount');
            $table->integer('price');
            $table->timestamps();
        });

        Schema::table('report_procurements', function (Blueprint $table) {
            $table->foreign('list_of_request_id')->references('id')->on('list_of_requests')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('book_failure_id')->references('id')->on('book_failures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_procurements');
    }
}
