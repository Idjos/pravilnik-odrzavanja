<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListOfRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_of_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('experts', ['IT', 'ODRŽAVANJE']);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('list_of_requests', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_of_requests');
    }
}
