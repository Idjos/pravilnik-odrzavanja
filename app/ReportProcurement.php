<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportProcurement extends Model
{
    /**
     * @var array
     */
    public $fillable = ['description', 'measurement_unit', 'book_failure_id', 'status', 'amount', 'price', 'user_id'];

    /**
     * appends
     */
    public $appends = ['total'];

    /**
     * list of requests relation
     */
    public function list_of_requests()
    {
        return $this->belongsTo('App\ListOfRequest');
    }

    /**
     * user relation
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * get total
     */
    public function getTotalAttribute() {
        $total = $this->amount * $this->price;
        return $total;
    }
}
