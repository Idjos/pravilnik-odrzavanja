<?php

namespace App\Http\Middleware;

use Closure;

class isReferentRacunovodja
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! ($request->user()->role == 'REFERENT' || $request->user()->role == 'RACUNOVODJA' || $request->user()->role == 'IT-SLUŽBA')) {
            return redirect('/');
        } else {
            return $next($request);
        }

    }
}
