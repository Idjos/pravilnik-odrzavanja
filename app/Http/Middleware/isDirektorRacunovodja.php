<?php

namespace App\Http\Middleware;

use Closure;

class isDirektorRacunovodja
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($request->user()->role == 'RAČUNOVOĐA' || $request->user()->role == 'DIREKTOR')) {
            return redirect('/');
        } else {
            return $next($request);
        }
    }
}
