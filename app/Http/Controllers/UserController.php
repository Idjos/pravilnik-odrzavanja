<?php

namespace App\Http\Controllers;

use App\DefaultAndGeneral;
use App\Http\Requests\NewUserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->get();
        return view('users', ['users' => $users, 'search' => null]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewUserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        if ($user->save()) {
            $message = array(
                'message' => 'Uspešno ste kreirali korisnika!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Korisnik nije kreiran!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }
        $user->role = $request->role;
        if ($user->save()) {
            $message = array(
                'message' => 'Uspešno ste izmenili podatke o korisniku!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Podaci o korisniku nisu izmenjeni!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->delete()) {
            $message = array(
                'message' => 'Uspešno ste obrisali korisnika!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Korisnik nije obrisan!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }

    /**
     * users search
     */
    public function searchUsers(Request $request)
    {
        if ($request->search) {
            $users = User::where('name', 'LIKE', '%'. $request->search. '%')->orWhere('email', 'LIKE', '%'. $request->search. '%')->orWhere('role', 'LIKE', '%'. $request->search. '%')->get();
        }
        else {
            $users = User::latest()->get();
        }
        return view('users', ['users' => $users, 'search' => $request->search]);
    }

    /**
     * settings get
     */
    public function settingsIndex()
    {
        $settings = DefaultAndGeneral::get();
        return view('settings', ['settings' => $settings]);
    }

    /**
     * edit settings
     */
    public function editSettings(Request $request) {
        $settings = DefaultAndGeneral::find($request->id);
        $settings->default_price = $request->default_price;
        if ($settings->save()) {
            $message = array(
                'message' => 'Uspešno ste izmenili podešavanja!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Podešavanja nisu izmenjena!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }
}
