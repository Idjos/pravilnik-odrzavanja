<?php

namespace App\Http\Controllers;

use App\ListOfRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ListOfRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now();

        $reports = ListOfRequest::whereHas('reportProcurements', function ($query) {
            return $query->where('status', 'ISPLAĆENO');
        })->with(['reportProcurements' => function($query)
        {
            return $query->where('status','ISPLAĆENO');
        }])
            ->with('user')
            ->whereBetween("created_at",[$start, $end])->latest()->get();

        return view('narucen-materijal', [
            'reports' => $reports,
            'start_date' => $start,
            'end_date' => $end,
            'search' => null,
        ]);
    }

    /**
     * moji-zahtevi search
     */
    public function searchOrderedMaterial(Request $request)
    {
        if ($request->start_date && $request->end_date && !$request->search) {
            $reports = ListOfRequest::whereHas('reportProcurements', function ($query) {
                return $query->where('status', 'ISPLAĆENO');
            })->with(['reportProcurements' => function($query)
            {
                return $query->where('status','ISPLAĆENO');
            }])
                ->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
        }

        elseif ($request->search && !$request->start_date && !$request->end_date) {
            $reports = ListOfRequest::with(['reportProcurements' => function($query)
            {
                $query->where('status','ISPLAĆENO');
            }])
                ->orWhere(function($q) use($request) {
                    return $q
                        ->where('name', 'LIKE', '%' . $request->search . '%');
                })->latest()->get();
        }

        elseif ($request->search && $request->start_date && $request->end_date) {
            $reports = ListOfRequest::whereHas('reportProcurements', function ($query) {
                return $query->where('status', 'ISPLAĆENO');
            })->with(['reportProcurements' => function($query)
            {
                return $query->where('status','ISPLAĆENO');
            }])
                ->whereBetween("created_at",[$request->start_date, $request->end_date])
                ->where(function($q) use($request) {
                    return $q
                        ->where('name', 'LIKE', '%' . $request->search . '%');
                })->latest()->get();
        }

        else {
            $reports = ListOfRequest::with(['reportProcurements' => function($query)
            {
                $query->where('status','ISPLAĆENO');
            }])->latest()->get();
        }

        return view('narucen-materijal', [
            'reports' => $reports,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'search' => $request->search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
