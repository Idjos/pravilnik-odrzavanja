<?php

namespace App\Http\Controllers;

use App\BookFailrule;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BookFailruleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now();

        if (Auth::user()->role == 'ZAPOSLENI' || Auth::user()->role == 'RAČUNOVOĐA' || Auth::user()->role == 'IT') {
            $book_failures = BookFailrule::with('user')->whereBetween("created_at",[$start, $end])->where('user_id', $id)->latest()->get();
        }
        elseif (Auth::user()->role == 'IT-SLUŽBA') {
            $book_failures = BookFailrule::with('user')->whereBetween("created_at",[$start, $end])->where('user_id', $id)->orWhere('experts', 'IT')->latest()->get();
        }
        elseif (Auth::user()->role == 'REFERENT') {
            $book_failures = BookFailrule::with('user')->whereBetween("created_at",[$start, $end])->where('user_id', $id)->orWhere('experts', 'ODRŽAVANJE')->latest()->get();
        }
        else {
            $book_failures = BookFailrule::with('user')->whereBetween("created_at",[$start, $end])->latest()->get();
        }

        return view('knjiga-kvarova',
            [
                'book_failures' => $book_failures,
                'end_date' => $end,
                'start_date' => $start,
                'user' => Auth::user(),
                'search' => null,
                'loading' => false
            ]);
    }

    /**
     * knjiga-kvarova search
     */
    public function getFailuresByDate(Request $request)
    {
        $id = Auth::user()->id;

        if (Auth::user()->role == 'ZAPOSLENI') {
            if ($request->start_date && $request->end_date && !$request->search) {
                $book_failures = BookFailrule::with('user')->where('user_id', $id)->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
            }
            elseif ($request->search && !$request->start_date && !$request->end_date) {
                dd($request->all());
                $book_failures = BookFailrule::with('user')->where('user_id', $id)->where(function ($q) use ($request) {
                    return $q
                        ->where('description', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('location', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('status', 'LIKE', '%' . $request->search . '%');
                })->latest()->get();
            }
            elseif ($request->search && $request->start_date && $request->end_date) {
                $book_failures = BookFailrule::with('user')->where('user_id', $id)->where(function ($q) use ($request) {
                    return $q
                        ->where('description', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('location', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('status', 'LIKE', '%' . $request->search . '%');
                })->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
            }
            else {
                $book_failures = BookFailrule::with('user')->where('user_id', $id)->latest()->get();
            }

        } else {
            if ($request->start_date && $request->end_date && !$request->search) {
                $book_failures = BookFailrule::with('user')
                    ->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
            }
            elseif ($request->search && !$request->start_date && !$request->end_date) {
                $book_failures = BookFailrule::with('user')->where(function ($q) use ($request) {
                    return $q
                        ->where('description', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('location', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('status', 'LIKE', '%' . $request->search . '%');
                })->latest()->get();
            }
            elseif ($request->search && $request->start_date && $request->end_date) {
                $book_failures = BookFailrule::with('user')->where(function ($q) use ($request) {
                    return $q
                        ->where('description', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('location', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('status', 'LIKE', '%' . $request->search . '%');
                })->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
            }
            else {
                $book_failures = BookFailrule::with('user')->latest()->get();
            }
        }

        return view('knjiga-kvarova', [
            'book_failures' => $book_failures,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'search' => $request->search,
            'loading' => false
        ]);
    }

    /**
     * moji-zahtevi all
     */
    public function getAllMyFailures()
    {
        $id = Auth::user()->id;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now();
        $book_failures = BookFailrule::with('user')->where('user_id', $id)->whereBetween("created_at",[$start, $end])->latest()->get();

        return view('moji-zahtevi',
            [
                'book_failures' => $book_failures,
                'end_date' => $end,
                'start_date' => $start,
                'user' => Auth::user(),
                'search' => null
            ]);
    }

    /**
     * moji-zahtevi search
     */
    public function getAllMyFailuresByDate(Request $request)
    {
        $id = Auth::user()->id;

        if ($request->start_date && $request->end_date && !$request->search) {
            $book_failures = BookFailrule::with('user')->where('user_id', $id)->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
        }
        elseif ($request->search && !$request->start_date && !$request->end_date) {
            $book_failures = BookFailrule::with('user')->where('user_id', $id)->where(function ($q) use ($request) {
                return $q
                    ->where('description', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('location', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('status', 'LIKE', '%' . $request->search . '%');
            })->latest()->get();
        }
        elseif ($request->search && $request->start_date && $request->end_date) {
            $book_failures = BookFailrule::with('user')->where('user_id', $id)->where(function ($q) use ($request) {
                return $q
                    ->where('description', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('location', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('status', 'LIKE', '%' . $request->search . '%');
            })->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
        }
        else {
            $book_failures = BookFailrule::with('user')->where('user_id', $id)->latest()->get();
        }

        return view('moji-zahtevi', [
            'book_failures' => $book_failures,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'search' => $request->search,
            'loading' => false
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emails_referent = User::where('role', 'REFERENT')->get();
        $emails_it = User::where('role', 'IT-SLUŽBA')->get();

        $failure = new BookFailrule();
        $failure->location = $request->location;
        $failure->description = $request->description;
        $failure->user_id = Auth::user()->id;
        $failure->status = 'POSLATO';
        $failure->experts = $request->experts;
        if ($failure->save()) {
            if ($request->experts == 'ODRŽAVANJE') {
                Mail::raw('Primljen je novi zahtev: '.$failure->description.'. Za detalje proverite http://odrzavanje.vtsappsteam.edu.rs/.', function ($message) use ($emails_referent, $failure) {
                    $message->from('odrzavanjevts@gmail.com', 'VTŠ Odrzavanja');
                    foreach ($emails_referent as $email) {
                        $message->to($email->email);
                    }
                    $message->subject('VTŠ Održavanje - Novi zahtev '.$failure->description);
                });
            }
            if ($request->experts == 'IT') {
                Mail::raw('Primljen je novi zahtev: '.$failure->description.'. Za detalje proverite http://odrzavanje.vtsappsteam.edu.rs/.', function ($message) use ($emails_it, $failure) {
                    $message->from('odrzavanjevts@gmail.com', 'VTŠ Odrzavanja');
                    foreach ($emails_it as $email) {
                        $message->to($email->email);
                    }
                    $message->subject('VTŠ Održavanje - Novi zahtev '.$failure->description);
                });
            }
            $message = array(
                'message' => 'Uspešno ste prijavili kvar!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Kvar nije prijavljen!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }

    /**
     * add failure details
     */
    public function addDetails(Request $request)
    {
        $user = Auth::user();
        $failure = BookFailrule::find($request->id);
        if ($user->role != 'REFERENT' && $request->experts != $failure->experts && ($failure->status!=$request->status || $request->work_description != $failure->work_description || $request->work_implemented_parts !=  $failure->work_implemented_parts)) {
            $message = array(
                'message' => 'Ne možete promeniti zaduženo lice i ostale parametre!',
                'type' => 'error'
            );
        }
        elseif ($user->role == 'REFERENT' && $request->experts != $failure->experts && ($failure->status!=$request->status || $request->work_description != $failure->work_description || $request->work_implemented_parts !=  $failure->work_implemented_parts)) {
            $message = array(
                'message' => 'Ne možete promeniti zaduženo lice i ostale parametre!',
                'type' => 'error'
            );
        }
        else {
            $failure->fill($request->all());
            if ($failure->save()) {
                $message = array(
                    'message' => 'Uspešno ste odgovorili prijavljen kvar!',
                    'type' => 'success'
                );
            }
            else {
                $message = array(
                    'message' => 'Neuspešan odgovor na prijavljen kvar!',
                    'type' => 'error'
                );
            }
        }
        return back()->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
