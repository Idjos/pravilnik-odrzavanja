<?php

namespace App\Http\Controllers;

use App\BookFailrule;
use App\DefaultAndGeneral;
use App\ListOfRequest;
use App\ReportProcurement;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PurchaseRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now();
        $user = Auth::user();

        if ($user->role == 'IT-SLUŽBA') {
            $expert = 'IT';
        }
        if ($user->role == 'REFERENT') {
            $expert = 'ODRŽAVANJE';
        }

        $procurement = ListOfRequest::where('experts',$expert)->whereHas('reportProcurements', function ($query) {
            return $query->where('status', '!=' ,'ISPLAĆENO');
        })
            ->with(['reportProcurements' => function($query)
            {
                $query->where('status', '!=' ,'ISPLAĆENO');
            }])->whereBetween("created_at",[$start, $end])->latest()->get();

        return view('poslati-zahtevi',
            [
                'procurement' => $procurement,
                'start_date' => $start,
                'end_date' => $end,
                'search' => null,
            ]);
    }

    /**
     * poslati-zahtevi search
     */
    public function searchSendRequests(Request $request)
    {
        $user = Auth::user();

        if ($user->role == 'IT-SLUŽBA') {
            $expert = 'IT';
        }
        if ($user->role == 'REFERENT') {
            $expert = 'ODRŽAVANJE';
        }

        if ($request->start_date && $request->end_date && !$request->search) {
            $procurement = ListOfRequest::where('experts',$expert)->whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
        }
        elseif ($request->search && !$request->start_date && !$request->end_date) {
            $procurement = ListOfRequest::where('experts',$expert)->where(function ($q) use ($request) {
                return $q
                    ->where('name', 'LIKE', '%' . $request->search . '%');
            })->latest()->get();
        }
        elseif ($request->search && $request->start_date && $request->end_date) {
            $procurement = ListOfRequest::where('experts',$expert)->where(function ($q) use ($request) {
                return $q
                    ->where('name', 'LIKE', '%' . $request->search . '%');
            })->latest()->get();
        }
        else {
            $procurement = ListOfRequest::where('experts',$expert)->latest()->get();
        }

        return view('poslati-zahtevi',
            [
                'procurement' => $procurement,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'search' => $request->search,
            ]);
    }

    /**
     * zahtev-nabavka get parameters
     */
    public function sendRequest()
    {
        $user = Auth::user();

        if ($user->role == 'IT-SLUŽBA') {
            $expert = 'IT';
        }
        if ($user->role == 'REFERENT') {
            $expert = 'ODRŽAVANJE';
        }
        $list_of_request = ListOfRequest::orderBy('updated_at', 'desc')->first();
        $book_failures = BookFailrule::where('status', 'POSLATO')->where('experts', $expert)->latest()->get();
        return view('zahtev-nabavka', ['book_failures' => $book_failures, 'list_of_request' => $list_of_request]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emails = User::where('role', 'DIREKTOR')->get();
        $user = Auth::user();

        if ($user->role == 'IT-SLUŽBA') {
            $expert = 'IT';
        }
        if ($user->role == 'REFERENT') {
            $expert = 'ODRŽAVANJE';
        }

        $list_of_requests = new ListOfRequest();
        $list_of_requests->user_id = $user->id;
        $list_of_requests->experts = $expert;
        $default_price = DefaultAndGeneral::select('default_price')->first();
        $last_list = ListOfRequest::orderBy('id', 'desc')->first();
        if ($last_list!=null) {
            $number = (int)$last_list->id;
            $list_of_requests->name = 'Zahtev broj '.($number+1).' '.$request->name;
        }
        else {
            $list_of_requests->name = 'Zahtev broj 1'.' '.$request->name;
        }
        if ($list_of_requests->save()) {
            foreach ($request->description as $index => $description) {
                $report = new ReportProcurement();
                $report->list_of_request_id = $list_of_requests->id;
                $report->description = $description;
                $report->book_failure_id = $request->book_failure_id[$index];
                $report->measurement_unit = $request->measurement_unit[$index];
                $report->amount = $request->amount[$index];
                $report->user_id = Auth::user()->id;
                $report->price = $request->price[$index];
                if (($request->price[$index] * $request->amount[$index]) >= $default_price->default_price) {
                    $report->status = 'ODOBRENO';
                }
                else {
                    $report->status = 'ČEKA SE ODGOVOR';
                    Mail::raw('Primljen je novi zahtev: '.$list_of_requests->name.'. Za detalje proverite http://odrzavanje.vtsappsteam.edu.rs/.', function ($message) use ($emails, $list_of_requests) {
                        $message->from('odrzavanjevts@gmail.com', 'VTŠ Odrzavanja');
                        foreach ($emails as $email) {
                            $message->to($email->email);
                        }
                        $message->subject('VTŠ Održavanje - Novi zahtev '.$list_of_requests->name);
                    });
                }
                $report->save();
            }
            $message = array(
                'message' => 'Uspešno ste poslali zahtev za nabavku!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Neuspešno ste poslali zahtev za nabavku!',
                'type' => 'error'
            );
        }

        return back()->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
