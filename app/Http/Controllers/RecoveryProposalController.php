<?php

namespace App\Http\Controllers;

use App\ListOfRequest;
use App\ReportProcurement;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RecoveryProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now();

        $procurement = ListOfRequest::whereHas('reportProcurements', function ($query) {
            return $query->where('status', '!=' ,'ISPLAĆENO');
        })
            ->with(['reportProcurements' => function($query)
            {
                $query->where('status', '!=' ,'ISPLAĆENO');
            }])->whereBetween("created_at",[$start, $end])->latest()->get();

        return view('predlog-oporavka',
            [
                'procurement' => $procurement,
                'start_date' => $start,
                'end_date' => $end,
                'search' => null,
            ]);
    }

    /**
     * lista-zahteva search
     */
    public function getFailuresByDateRecovery(Request $request)
    {
        if ($request->start_date && $request->end_date && !$request->search) {
            $procurement = ListOfRequest::whereBetween("created_at",[$request->start_date, $request->end_date])->latest()->get();
        }
        elseif ($request->search && !$request->start_date && !$request->end_date) {
            $procurement = ListOfRequest::where(function ($q) use ($request) {
                return $q
                    ->where('name', 'LIKE', '%' . $request->search . '%');
            })->latest()->get();
        }
        elseif ($request->search && $request->start_date && $request->end_date) {
            $procurement = ListOfRequest::where(function ($q) use ($request) {
                return $q
                    ->where('name', 'LIKE', '%' . $request->search . '%');
            })->latest()->get();
        }
        else {
            $procurement = ListOfRequest::latest()->get();
        }

        return view('predlog-oporavka',
            [
                'procurement' => $procurement,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'search' => $request->search,
            ]);
    }

    /**
     * answer on request
     */
    public function statusAnswer(Request $request)
    {
        $ids = $request->id;
        $more_status = $request->status;
        $count = count($ids);
        for ($i=0; $i<$count; $i++) {
            $answer = ReportProcurement::find($ids[$i]);
            $answer->status = $more_status[$i];
            if ($answer->save()) {
                $message = array(
                    'message' => 'Uspešno ste odgovorili na zahtev!',
                    'type' => 'success'
                );
            }
            else {
                $message = array(
                    'message' => 'Neuspešan odgovor na zahtev!',
                    'type' => 'error'
                );
            }
        }
        return back()->with($message);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
