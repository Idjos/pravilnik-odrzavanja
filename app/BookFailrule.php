<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookFailrule extends Model
{
    /**
     * @var array
     */
    public $fillable = ['description', 'location', 'user_id', 'status', 'work_description', 'work_implemented_parts', 'work_comment', 'experts'];

    /**
     * appends
     */
    public $appends = ['total_price'];

    /**
     * table
     */
    public $table = 'book_failures';

    /*
     * Append total amount of all purchased items
     */

    /**
     * user relation
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * report procurements relation
     */
    public function reportProcurements() {
        return $this->belongsToMany('App\ReportProcurement');
    }

    /**
     * total for single stuff
     */
    public function getTotalPriceAttribute() {
        $total_price = 0;
        $reports = ReportProcurement::where('book_failure_id', $this->id)->get();
        foreach ($reports as $report) {
            $total_price += $report->amount * $report->price;
        }
        return $total_price;
    }
}
