<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultAndGeneral extends Model
{
    /**
     * @var array
     */
    public $fillable = ['default_price'];
}
