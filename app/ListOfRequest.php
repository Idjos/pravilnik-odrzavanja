<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListOfRequest extends Model
{
    /**
     * @var array
     */
    public $fillable = ['name', 'user_id'];

    /**
     * append
     */
    public $appends = ['total'];

    /*
     * Append total amount of all purchased items
     */

    /**
     * user relation
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * report procurements relation
     */
    public function reportProcurements() {
        return $this->hasMany('App\ReportProcurement');
    }

    /**
     * get all totals
     */
    public function getTotalAttribute() {
        $total = $this->reportProcurements->sum('total');
        return $total;
    }

}
